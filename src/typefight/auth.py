from flask import Blueprint, abort, redirect, render_template, request, url_for
from flask_login import login_required, logout_user

from ._utils import is_safe_url
from .extensions import db
from .forms import LoginForm, RegisterForm
from .models import Player

bp = Blueprint("auth", __name__, url_prefix="/auth")


@bp.route("/register", methods=["GET", "POST"])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        name = form.username.data
        password = form.password.data
        country = form.country.data

        new_player = Player(name, password, country)
        db.session.add(new_player)
        db.session.commit()

        return redirect(url_for("auth.login"))
    return render_template("auth.html", register_form=form, login_form=LoginForm())


@bp.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        next = request.args.get("next")
        if not is_safe_url(next):
            return abort(400)

        return redirect(next or url_for("game.home"))

    return render_template(
        "auth.html", register_form=RegisterForm(), login_form=form, login=True
    )


@bp.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
    logout_user()
    return redirect(url_for("game.home"))
