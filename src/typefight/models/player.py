import secrets
import uuid

from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import CHAR, UUID, VARCHAR
from werkzeug.security import generate_password_hash

from ..extensions import db


class Player(db.Model, UserMixin):
    player_uid = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    player_name = db.Column(VARCHAR(15), nullable=False)
    country = db.Column(VARCHAR())
    salt = db.Column(CHAR(16), nullable=False)
    pass_hash = db.Column(VARCHAR(), nullable=False)
    db.UniqueConstraint(player_name)

    def __init__(self, name, password, country=None) -> None:
        salt = secrets.token_hex(8)
        pass_salt = password + salt
        pass_hash = generate_password_hash(pass_salt)

        self.player_name = name
        self.country = country
        self.salt = salt
        self.pass_hash = pass_hash

    def __repr__(self) -> str:
        return f"<Player '{self.player_name}'>"

    def get_id(self):
        try:
            return str(self.player_uid)
        except:
            raise NotImplementedError("Something went wrong in Player.get_id")
