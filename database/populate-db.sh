function insert_test_users() {
  psql -v ON_ERROR_STOP=1 -U "$POSTGRES_USER" "$POSTGRES_DB" << EOF
  INSERT INTO player(player_uid, player_name, country, salt, pass_hash)
  VALUES
    ('292a485f-a56a-4938-8f1a-bbbbbbbbbbb1', 'Eminem', 'United States', 'aaaaaaaa', 'bbbbbbbb'),
    (uuid_generate_v4(), 'Slim Shady', NULL, 'aaaaaaaa', 'bbbbbbbb'),
    ('292a485f-a56a-4938-8f1a-bbbbbbbbbbb2', 'Kendrick', 'United States', 'aaaaaaaa', 'bbbbbbbb'),
    (uuid_generate_v4(), 'King Kunta', 'United States', 'aaaaaaaa', 'bbbbbbbb');
  SELECT * FROM player;
EOF
}

function insert_test_quotes() {
  psql -v ON_ERROR_STOP=1 -U "$POSTGRES_USER" "$POSTGRES_DB" << EOF
  INSERT INTO quote(quote_uid, quote)
  VALUES
    ('292a485f-a56a-4938-8f1a-bbbbbbbbbbb3', 'The quick fox jumps over the lazy dog.'),
    ('292a485f-a56a-4938-8f1a-bbbbbbbbbbb4', 'Out of the frying pan, into the fire.');
  SELECT * FROM quote;
EOF
}

function insert_test_scores() {
  psql -v ON_ERROR_STOP=1 -U "$POSTGRES_USER" "$POSTGRES_DB" << EOF
  INSERT INTO score(player_uid, quote_uid, score)
  VALUES
    ('292a485f-a56a-4938-8f1a-bbbbbbbbbbb1', '292a485f-a56a-4938-8f1a-bbbbbbbbbbb3', 12.5),
    ('292a485f-a56a-4938-8f1a-bbbbbbbbbbb2', '292a485f-a56a-4938-8f1a-bbbbbbbbbbb4', 5.6);
  SELECT * FROM score;
EOF
}

BLUE='\033[0;34m'
db_name=$(printenv POSTGRES_DB)

if [ "$db_name" = "dev" ]; then
  # Populate the database with test data if the environment is "dev"
  printf "
${BLUE}##############################################
${BLUE}#     Populating database with test data     #
${BLUE}##############################################\n
  "
  insert_test_users
  insert_test_quotes
  insert_test_scores
fi
