from flask import (
    Blueprint,
    abort,
    current_app,
    g,
    jsonify,
    render_template,
    request,
    session,
)
from flask_login import current_user
from sqlalchemy.sql import func

from ._utils import is_safe_url
from .extensions import db
from .models import Player, Quote, Score

bp = Blueprint("game", __name__)


@bp.route("/")
def home():
    return render_template("index.html")


@bp.route("/highscores", methods=["GET"])
@bp.route("/highscores/<float:score>", methods=["POST"])
def handle_highscores(score=None):
    quote_id = g.get("quote_uid")
    server_response = {
        "success": False,
        "message": "You need to be login to save your score",
    }

    if request.method == "GET":
        scores_table = (
            Score.query.filter_by(quote_uid=quote_id)
            .join(Player, Score.player_uid == Player.player_uid)
            .add_columns(Score.score, Player.player_name, Player.country)
            .all()
        )
        scores_table = [
            {"score": float(score[1]), "player_name": score[2], "country": score[3]}
            for score in scores_table
        ]

        return jsonify(sorted(scores_table, key=lambda item: item["score"]))

    if not current_user.is_authenticated:
        # this'll return a 401(Unauthorized) code
        return current_app.login_manager.unauthorized()

    existing_score = Score.query.get((str(current_user.player_uid), quote_id))

    if not existing_score:
        new_score = Score(
            player_uid=current_user.player_uid, quote_uid=quote_id, score=score
        )
        db.session.add(new_score)
        db.session.commit()

        server_response["success"] = True
        server_response["message"] = f"Your new score ({score}) has been saved."
    elif score < existing_score.score:
        existing_score.score = score
        db.session.add(existing_score)
        db.session.commit()

        server_response["success"] = True
        server_response["message"] = "New record!"
    else:
        server_response["success"] = True
        server_response["message"] = "Too slow!"

    next = request.args.get("next")
    if not is_safe_url(next):
        return abort(400)

    return server_response


@bp.route("/quote")
def get_quote():
    # this seemed to be the fastest method to get random quote, according to:
    # https://stackoverflow.com/questions/60805/getting-random-row-through-sqlalchemy
    quote_count = db.session.query(func.count(Quote.quote_uid))
    quote = Quote.query.offset(func.floor(func.random() * quote_count)).first()
    session["quote_uid"] = str(quote.quote_uid)

    return jsonify({"id": quote.quote_uid, "quote": quote.quote})


@bp.before_request
def load_current_quote():
    quote = session.get("quote_uid")

    g.quote_uid = None if quote is None else quote
