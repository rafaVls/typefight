import uuid

from sqlalchemy.dialects.postgresql import UUID, VARCHAR

from ..extensions import db


class Quote(db.Model):
    quote_uid = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    quote = db.Column(VARCHAR(), nullable=False)
    db.UniqueConstraint(quote)

    def __repr__(self) -> str:
        return f"<Quote '{self.quote}'>"
