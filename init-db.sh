#!/bin/bash

function build_container() {
  docker-compose build
}

function run_container() {
  # check to see if the typefight-db container already exists
  cont_exists=$(docker images typefight-db:1.0 | grep -i typefight | awk '{ print $1 }')
  if [ "$cont_exists" != "typefight-db" ]; then
    build_container
  fi

  BLUE='\033[0;34m'
  NC='\033[0m'
  printf "\n\n
########    Using the ${BLUE}$1${NC} file to build database    ########
  \n\n"

  docker-compose --env-file=$1 up -d
}

NO_ENV='Try "dev", "prod", or "test" without quotation marks.'
if [ -z "$1" ]; then
  echo "No environment provided. $NO_ENV"
  exit 1
fi

case $1 in
  dev)
    run_container .dev.env
    exit 0
    ;;

  # test)
  #   run_container .test.env
  #   exit 0
  #   ;;

  prod)
    run_container .env
    exit 0
    ;;

  *)
    echo "Not an accepted parameter. $NO_ENV"
    exit 1
    ;;
esac
