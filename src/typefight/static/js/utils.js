const registerBtn = document.getElementById("register-btn");
const registerForm = document.getElementById("register");
const loginBtn = document.getElementById("login-btn");
const loginForm = document.getElementById("login");

function switchForm(buttonElement, elementToMove) {
  const buttonValue = buttonElement.value;

  elementToMove.style.transform = `translateX(${getTranslateAmount(buttonValue)})`;
  toggleClasses(buttonValue);
}

function getTranslateAmount(buttonValue) {
  if (buttonValue === "login") {
    return "40vw";
  } else if (buttonValue === "register") {
    return "0";
  }
}

function toggleClasses(buttonValue) {
  const className = "hidden";
  const loginValue = buttonValue === "login";
  const registerValue = buttonValue === "register";

  loginValue && toggleClass(loginForm);
  registerValue && toggleClass(registerForm);

  // Timeout so the form isn't hidden immediately. Makes the transition smoother
  setTimeout(() => {
    toggleClass(registerBtn);
    toggleClass(loginBtn);

    loginValue && toggleClass(registerForm);
    registerValue && toggleClass(loginForm);
  }, 300);
}

function toggleClass(element, className = "hidden") {
  element.classList.toggle(className);
}

export { switchForm };
