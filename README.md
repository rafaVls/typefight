# Typefight
Typefight is a typing game where you get a random sentence, type it in, and see your score, as well as other people's score.

This app comes from my desire to learn about databases and how to interact with them. In this case, I'm using python's Flask framework for the backend.

## Features
The game randomly assigns the player a short sentence, which they must type (including characters like spaces, periods, commas, etc.). After which, the player gets the time it took them to type in that sentence, and how long it takes other players who've typed the same sentence.

## Installing locally
### Requirements:
- Git
- Docker
- Python3

### Installation process

Clone the repo to your machine

```bash
git clone https://gitlab.com/rafaVls/typefight.git
```

Change into the project directory and change the name of the .env file
```bash
cd typefight
mv .example.env .dev.env
```

Run the init-db script with the dev option to fetch and initialize the docker containers
```bash
source ./init-db.sh dev
```

Change into the flask directory, activate a virtual environment (optional) and install requirements
```bash
cd src/
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

Run the flask app
```bash
python3 start.py
```

Head over to [localhost:5000](http://localhost:5000), have fun!

## Screenshots
### Landing page
![Landing page for the game Typefight](/src/typefight/static/assets/landing-page.png)
---
### Game section
![Game section for the game Typefight](/src/typefight/static/assets/game-page.png)

### Mobile view
![Mobile view for the game Typefight](/src/typefight/static/assets/mobile-view.png)

## Tech stack
- HTML, CSS & Javascript
- Docker containers
- Postgresql 13
- Python 3.8
    - Flask
        - Flask-SQLAlchemy
        - Flask-Login
        - Flask-WTF
