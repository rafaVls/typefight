import json
import os
from urllib.parse import urljoin, urlparse

from flask import request


def get_countries_list() -> list:
    filepath = os.path.join(os.path.dirname(__file__), "static", "countries.json")

    try:
        with open(filepath, "r") as file:
            countries = json.loads(file.read())
            file.close()

        countries = [(countries[i], countries[i]) for i in range(0, len(countries))]
        countries.insert(0, ("", "Select a country..."))
        return countries

    except FileNotFoundError:
        raise FileNotFoundError(f"file {filepath} not found")


def is_safe_url(target):
    # as per https://flask-login.readthedocs.io/en/latest/#login-example
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc
