from flask import Flask

from . import api, auth
from .extensions import db, login_manager
from .models import Player


def create_app(config_filename="dev.cfg"):
    # configure Flask app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile(config_filename)

    # initialize database and flask-login
    db.init_app(app)
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return Player.query.get(str(user_id))

    # register blueprints
    app.register_blueprint(api.bp)
    app.register_blueprint(auth.bp)

    return app
