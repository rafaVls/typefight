from flask_login import login_user
from flask_wtf import FlaskForm
from werkzeug.security import check_password_hash
from wtforms import PasswordField, SelectField, StringField
from wtforms.validators import DataRequired, EqualTo, Length, Regexp, ValidationError

from ._utils import get_countries_list
from .models import Player


# ==========[ REGISTRATION FORM ]========== #
class RegisterForm(FlaskForm):
    username = StringField(
        "Username",
        [
            DataRequired("A username is required"),
            Length(max=15, message="Keep the username under 15 characters, please"),
            Regexp(r"^\w+$", message="Only letters, numbers and underscores"),
        ],
        render_kw={"placeholder": "rafaVls"},
    )

    password = PasswordField(
        "Password",
        [
            DataRequired("A password is required"),
            Length(min=6, message="Password must be at least 6 characters long"),
            EqualTo("confirm", message="Passwords don't match"),
        ],
        render_kw={"placeholder": "safePassword123"},
    )

    confirm = PasswordField(
        "Confirm password", render_kw={"placeholder": "safePassword123"}
    )

    country = SelectField("Country", choices=get_countries_list())

    def validate_username(self, username):
        username_exists = Player.query.filter_by(player_name=username.data).first()

        if username_exists:
            raise ValidationError(f'Player name "{username.data}" already exists')

    def validate_country(self, country):
        countries = [country[0] for country in get_countries_list()]
        if country.data not in countries:
            raise ValidationError(f"Invalid country, please select one from the list")


# ==========[ REGISTRATION FORM ]========== #
class DatabaseCheck(object):
    def __init__(self):
        pass

    def __call__(self, form, field):
        player = Player.query.filter_by(player_name=form.username.data).first()

        # if player exists in the database, check password
        if player is None:
            if field.id == "username":
                raise ValidationError(f'Username "{field.data}" not found')
        else:
            if field.id == "password":
                if not check_password_hash(player.pass_hash, field.data + player.salt):
                    raise ValidationError("Incorrect password")
                else:
                    login_user(player)


class LoginForm(FlaskForm):
    username = StringField(
        "Username",
        [
            DataRequired("A username is required"),
            Length(max=15, message="Usernames are below 15 characters"),
            DatabaseCheck(),
        ],
    )

    password = PasswordField(
        "Password",
        [DataRequired("A password is required"), DatabaseCheck()],
        render_kw={"placeholder": "safePassword123"},
    )
