from sqlalchemy import CheckConstraint
from sqlalchemy.dialects.postgresql import UUID

from ..extensions import db


class Score(db.Model):
    player_uid = db.Column(
        UUID(as_uuid=True),
        db.ForeignKey("player.player_uid", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    )
    quote_uid = db.Column(
        UUID(as_uuid=True),
        db.ForeignKey("quote.quote_uid", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    )
    score = db.Column(db.Numeric(6, 2), CheckConstraint("score>0"), nullable=False)

    def __repr__(self) -> str:
        return f"""
<Score
    player_uid: {self.player_uid}
    quote_uid: {self.quote_uid}
    score: {self.score}
>"""
