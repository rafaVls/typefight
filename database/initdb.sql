CREATE TABLE IF NOT EXISTS player(
  player_uid UUID PRIMARY KEY,
  player_name VARCHAR(15) NOT NULL,
  country VARCHAR,
  salt CHAR(16) NOT NULL,
  pass_hash VARCHAR NOT NULL,
  UNIQUE(player_name)
);

CREATE TABLE IF NOT EXISTS quote(
  quote_uid UUID PRIMARY KEY,
  quote VARCHAR NOT NULL,
  UNIQUE(quote)
);

CREATE TABLE IF NOT EXISTS score(
  player_uid UUID REFERENCES player(player_uid) ON DELETE CASCADE,
  quote_uid UUID REFERENCES quote(quote_uid) ON DELETE CASCADE,
  score NUMERIC(6,2) NOT NULL CHECK(score > 0),
  PRIMARY KEY(player_uid, quote_uid)
);

SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_name = 'player';
SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_name = 'quote';
SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_name = 'score';
